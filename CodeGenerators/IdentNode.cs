﻿namespace EnforceGenerator.CodeGenerators;

public class IdentNode: BaseNode
{
    public string Value { get; set; }
    public override string ToString()
    {
        return Value;
    }
}
