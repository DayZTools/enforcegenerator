﻿using System.Text;

namespace EnforceGenerator.CodeGenerators;

public class ReturnNode: BaseNode
{
    public BaseNode? ReturnValue { get; set; }

    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("return");
        if (ReturnValue != null)
        {
            sb.Append(' ');
            sb.Append(ReturnValue);
        }
        return sb.ToString();
    }
}
