﻿using System.Text;

namespace EnforceGenerator.CodeGenerators;

public class IfNode: BaseNode
{
    public BaseNode Expression { get; set; }
    public List<BaseNode> Block = new();
    public List<IfElseNode> IfElses { get; set; } = new();
    public List<BaseNode> Else { get; set; } = new();

    public IfNode()
    {
        needSemi = false;
    }

    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("if (");
        sb.Append(Expression);
        sb.AppendLine(") {");
        UpIdent();
        foreach (var node in Block)
        {
            node.PutLine(sb);
        }
        DownIdent();
        Append(sb, "}");

        if (IfElses.Count > 0)
        {
            UpIdent();
            foreach (var node in Else)
            {
                sb.Append(node);
            }
            DownIdent();
        }

        if (Else.Count > 0)
        {
            UpIdent();
            sb.AppendLine("else {");
            foreach (var node in IfElses)
            {
                sb.Append(node);
            }

            DownIdent();
            Append(sb, "}");
        }

        sb.AppendLine();

        return sb.ToString();
    }
}


public class IfElseNode : BaseNode
{
    public BaseNode Expression { get; set; }
    public List<BaseNode> Block = new();
    
    public IfElseNode()
    {
        needSemi = false;
    }

    public override string ToString()
    {
        var sb = new StringBuilder();
        Append(sb, "else if (");
        sb.Append(Expression);
        sb.AppendLine(") {");
        UpIdent();
        foreach (var node in Block)
        {
            sb.Append(node);
        }
        DownIdent();
        Append(sb, "}");

        return sb.ToString();
    }
}
