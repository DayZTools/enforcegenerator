﻿namespace EnforceGenerator.CodeGenerators;

public class StringNode: BaseNode
{
    public string Value { get; set; }

    public override string ToString()
    {
        return "\"" + Value + "\"";
    }
}
