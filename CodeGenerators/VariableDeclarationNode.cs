﻿using System.Text;

namespace EnforceGenerator.CodeGenerators;

public class VariableDeclarationNode: BaseNode
{
    public string? Modifier { get; set; }
    public string Type { get; set; }
    public string Name { get; set; }
    public BaseNode? DefaultValue { get; set; }

    public override string ToString()
    {
        var sb = new StringBuilder();
        if (Modifier != null)
        {
            sb.Append(Modifier);
            sb.Append(' ');
        }

        sb.Append(Type);
        sb.Append(' ');
        sb.Append(Name);
        if (DefaultValue != null)
        {
            sb.Append(" = ");
            sb.Append(DefaultValue);
        }

        return sb.ToString();
    }
}
