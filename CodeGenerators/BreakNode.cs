﻿namespace EnforceGenerator.CodeGenerators;

public class BreakNode: BaseNode
{
    public override string ToString()
    {
        return "break";
    }
}
