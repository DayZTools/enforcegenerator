﻿using System.Text;

namespace EnforceGenerator.CodeGenerators;

public class BaseNode
{
    private static int _ident = 0;

    protected bool needSemi = true;
    
    public override string ToString()
    {
        throw new NotImplementedException();
    }

    protected static void UpIdent()
    {
        _ident += 1;
    }

    protected static void DownIdent()
    {
        _ident -= 1;
        if (_ident < 0)
        {
            _ident = 0;
        }
    }
    
    protected static string GetIdent()
    {
        return new string(' ', _ident * 4);
    }

    public void PutIdent(StringBuilder sb)
    {
        sb.Append(GetIdent());
    }

    public void PutLine(StringBuilder sb)
    {
        sb.Append(GetIdent());
        sb.Append(this);
        if (needSemi)
        {
            sb.AppendLine(";");
        }
    }

    public void AppendLine(StringBuilder sb, string str)
    {
        sb.Append(GetIdent());
        sb.AppendLine(str);
    }
    
    public void Append(StringBuilder sb, string str)
    {
        sb.Append(GetIdent());
        sb.Append(str);
    }
}
