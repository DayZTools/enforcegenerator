﻿using System.Text;

namespace EnforceGenerator.CodeGenerators;

public class ClassNode: BaseNode
{
    public string? Modifier { get; set; }
    public bool IsModded { get; set; } = false;
    public IdentNode Name { get; set; }
    public IdentNode? ExtendedFrom { get; set; }
    public List<VariableDeclarationNode> Variables { get; set; } = new();
    public List<FunctionDefinitionNode> FunDefinitions { get; set; } = new();

    public override string ToString()
    {
        var sb = new StringBuilder();
        if (IsModded)
        {
            sb.Append("modded ");
        }
        if (Modifier != null)
        {
            sb.Append(Modifier);
            sb.Append(' ');
        }
        sb.Append("class ");
        sb.Append(Name);

        if (ExtendedFrom != null)
        {
            sb.Append(" : ");
            sb.Append(ExtendedFrom);
        }
        
        sb.AppendLine(" {");
        UpIdent();
        foreach (var decl in Variables)
        {
            decl.PutLine(sb);
        }

        sb.AppendLine();
        
        foreach (var fun in FunDefinitions)
        {
            fun.PutLine(sb);
        }

        DownIdent();
        sb.AppendLine("}");
        return sb.ToString();
    }
}
