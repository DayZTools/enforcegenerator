﻿using System.Text;

namespace EnforceGenerator.CodeGenerators;

public class FunctionDefinitionNode : FunctionDeclarationNode
{
    public List<BaseNode> Code { get; set; } = new();

    public FunctionDefinitionNode()
    {
        needSemi = false;
    }

    public override string ToString()
    {
        var sb = new StringBuilder(base.ToString());
        sb.AppendLine(" {");
        UpIdent();
        foreach (var node in Code)
        {
            node.PutLine(sb);
        }

        // default return
        if (Code.Count == 0 && Type != GenBasicTypes.Void)
        {
            if (Type == GenBasicTypes.Int)
            {
                AppendLine(sb, "return 0;");
            }
            else if (Type == GenBasicTypes.Float)
            {
                AppendLine(sb, "return 0.0;");
            }
            else if (Type == GenBasicTypes.Bool)
            {
                AppendLine(sb, "return false;");
            }
            else
            {
                AppendLine(sb, "return null;");
            }
        }

        DownIdent();
        AppendLine(sb, "}\n");
        return sb.ToString();
    }
}
