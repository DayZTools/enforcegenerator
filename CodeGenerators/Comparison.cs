﻿namespace EnforceGenerator.CodeGenerators;

public class CompNode : BaseNode
{
    public BaseNode Left { get; set; }
    public BaseNode Right { get; set; }
    protected string comparer;
    public override string ToString()
    {
        return Left + " " + comparer + " " + Right;
    }
}

public class EqualNode: CompNode
{
    public EqualNode()
    {
        comparer = "==";
    }
}
public class NotEqualNode: CompNode
{
    public NotEqualNode()
    {
        comparer = "!=";
    }
}

