﻿using System.Text;

namespace EnforceGenerator.CodeGenerators;

public class FunctionDeclarationNode: BaseNode
{
    public string? Modifier { get; set; }
    public string Type { get; set; }
    public string Name { get; set; }

    public List<VariableDeclarationNode> Params { get; set; } = new();

    public override string ToString()
    {
        var sb = new StringBuilder();
        if (Modifier != null)
        {
            sb.Append(Modifier);
            sb.Append(' ');
        }

        sb.Append(Type);
        sb.Append(' ');
        sb.Append(Name);
        sb.Append('(');
        for (var i = 0; i < Params.Count; i++)
        {
            var variable = Params[i];
            sb.Append(variable);
            if (i != Params.Count - 1)
            {
                sb.Append(", ");
            }
        }

        sb.Append(')');

        return sb.ToString();
    }
}
