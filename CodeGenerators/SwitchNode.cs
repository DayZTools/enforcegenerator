﻿using System.Text;

namespace EnforceGenerator.CodeGenerators;

public class CaseNode : BaseNode
{
    public BaseNode Expression { get; set; }
    public List<BaseNode> Code { get; set; } = new();

    public CaseNode()
    {
        needSemi = false;
    }

    public override string ToString()
    {
        var sb = new StringBuilder();
        PutIdent(sb);
        sb.Append("case ");
        sb.Append(Expression);
        sb.AppendLine(":");
        UpIdent();
        foreach (var node in Code)
        {
            node.PutLine(sb);
        }
        DownIdent();

        return sb.ToString();
    }
}

public class SwitchNode: BaseNode
{
    public BaseNode Expression { get; set; }
    public List<CaseNode> Cases { get; set; } = new();

    public SwitchNode()
    {
        needSemi = false;
    }

    public override string ToString()
    {
        var sb = new StringBuilder();

        sb.Append("switch(");
        sb.Append(Expression);
        sb.AppendLine(") {");
        UpIdent();
        
        foreach (var node in Cases)
        {
            sb.Append(node);
        }

        DownIdent();
        AppendLine(sb, "}");

        return sb.ToString();
    }
}
