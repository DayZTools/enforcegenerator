﻿using System.Text;

namespace EnforceGenerator.CodeGenerators;

public class CallNode: BaseNode
{
    public IdentNode? Object { get; set; }
    public IdentNode Name { get; set; }
    public List<BaseNode> Args { get; set; } = new();
    public override string ToString()
    {
        var sb = new StringBuilder();
        if (Object != null)
        {
            sb.Append(Object);
            sb.Append('.');
        }
        sb.Append(Name);
        sb.Append('(');
        for (var i = 0; i < Args.Count; i++)
        {
            sb.Append(Args[i]);
            if (i != Args.Count - 1)
            {
                sb.Append(", ");
            }
        }
        sb.Append(')');
        return sb.ToString();
    }
}
