﻿namespace EnforceGenerator.CodeGenerators;

public static class GenBasicTypes
{
    public static string Void = "void";
    public static string Int = "int";
    public static string Float = "float";
    public static string String = "string";
    public static string Bool = "bool";
}
