﻿namespace EnforceGenerator.CodeGenerators;

public class AssignmentNode: BaseNode
{
    public IdentNode Left { get; set; }
    public BaseNode Right { get; set; }

    public override string ToString()
    {
        return Left + " = " + Right;
    }
}
